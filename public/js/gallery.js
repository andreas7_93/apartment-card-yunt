$('.gallery__wrapper').magnificPopup({
    delegate: 'a',
    type: 'image',
    fixedContentPos: true,
    gallery: {
        enabled: true,
        tCounter: '<span class="mfp-counter">%curr% из %total%</span>',
    },
});
