$(document).ready(function(){

    let floorTabImg = $("a[data-content-tab='floor'] .tabs__content-item-img")[0],
        strTab = $("a[data-content-tab='floor'] area").attr('coords');

    canvasStroke('floor-tab-canvas', floorTabImg, strTab);


    function canvasStroke(canvasID, canvasImg, str) {
        if(canvasID && canvasImg && str) {
            let canvasTarget = $("#" + canvasID),
                coords = str.split(',');

            canvasTarget.attr('width', canvasImg.naturalWidth);
            canvasTarget.attr('height', canvasImg.naturalHeight) ;

            let canvas = document.getElementById(canvasID),
                context = canvas.getContext("2d");

            context.beginPath();
            context.moveTo(coords[0], coords[1]);
            for(let i = 2; i < coords.length; i++) {
                if(i % 2 === 0){
                    context.lineTo(coords[i], coords[i + 1]);
                }
            }
            context.closePath();
            context.lineWidth = 8;
            context.strokeStyle = "#40A035";
            context.stroke();
        }
    }

    let cardOptions = $('.card__options__wrapper')

    cardOptions.on('click', function(){
        let timOpts = 0;
        cardOptions.addClass('active');
        timOpts = setTimeout(function(){
            cardOptions.removeClass('active');
        }, 3000)

    })


    let popupShow = $('.popup__show'),
        popupClose = $('.popup__close'),
        popupBg = $('.popup__bg')

    popupShow.on('click', function(){
       let key = $(this).attr('data-popup-key');
       $('.popup__wrapper[data-popup-value=' + key + ']').addClass('popup__wrapper-active')
    })
    popupClose.on('click', function(){
        $('.popup__wrapper-active').removeClass('popup__wrapper-active')
    })
    popupBg.on('click', function(){
        $('.popup__wrapper-active').removeClass('popup__wrapper-active')
    })


    let conceptType = $('.section-3 .concept__type');

    conceptType.on('click', function(){
        $(".section-3 .concept__type-active").removeClass('concept__type-active');
        $(".section-3 .slider-active").removeClass('slider-active');
        $(this).addClass('concept__type-active');
        $(".section-3 [data-concept-content="+ $(this).attr('data-concept-type') +"]").addClass('slider-active');
    })


})