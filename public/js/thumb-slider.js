$(document).ready(function(){

    $('.lightbox').magnificPopup({
        type: 'image'
    });

    let floorTabImg = $("a[data-content-tab='floor'] .tabs__content-item-img")[0],
        strTab = $("a[data-content-tab='floor'] area").attr('coords');

    canvasStroke('floor-tab-canvas', floorTabImg, strTab);


    function canvasStroke(canvasID, canvasImg, str) {
        if(canvasID && canvasImg && str) {
            let canvasTarget = $("#" + canvasID),
                coords = str.split(',');

            canvasTarget.attr('width', canvasImg.naturalWidth);
            canvasTarget.attr('height', canvasImg.naturalHeight) ;

            let canvas = document.getElementById(canvasID),
                context = canvas.getContext("2d");

            context.beginPath();
            context.moveTo(coords[0], coords[1]);
            for(let i = 2; i < coords.length; i++) {
                if(i % 2 === 0){
                    context.lineTo(coords[i], coords[i + 1]);
                }
            }
            context.closePath();
            context.lineWidth = 8;
            context.strokeStyle = "#40A035";
            context.stroke();
        }
    }

    let floorImg = $('#floor-modal img')[0],
        str = $("#floor-modal area").attr('coords');

    $('.modal').magnificPopup({
        callbacks: {
            open: function () {
                canvasStroke('floor-canvas', floorImg, str);
            }
        }
    });

    let slider = $('.thumb-slider');

    if(slider) {

        $('.thumb-slider__slides').magnificPopup({
            delegate: 'a',
            type: 'image',
            fixedContentPos: true,
            gallery: {
                enabled: true,
                tCounter: '<span class="mfp-counter">%curr% из %total%</span>',
            },
        });

        let items = $('.thumb-slider__slides > a');

        $('.tabs').addClass('thumb-slider-init');
        slider.append('<div class="thumb-slider__thumbnails"/>');

        items.each(function (index) {
            $('.thumb-slider__thumbnails').append(
                '<div ' +
                'class="thumb-slider__thumbnails-item" ' +
                'data-thumb="' +
                index + '" ' +
                'style="background-image: url(' +
                $(this).find('img').attr('src') +
                ')"' +
                '/>'
            );
        })

        $('.thumb-slider__thumbnails-item').on('click', function () {
            $(items).removeClass('active');
            const target = $(items)[$(this).attr('data-thumb')];
            $(target).addClass('active');
        })
    }
})