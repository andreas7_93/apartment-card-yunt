$(document).ready(function(){
    let tabItems = $('.tabs__wrapper .tab__item');

    tabItems.on('click', function(e){
        $('.tab__item-active').removeClass('tab__item-active');
        $(this).addClass('tab__item-active');
        $('.tabs__content-active').removeClass("tabs__content-active");
        $('[data-content-tab=' + $(this).attr('data-tab-title') + ']').addClass('tabs__content-active');
    })

    let tabSelect = $(".tab-select");

    tabSelect.selectmenu({
        open: function (event, ui) {
            $('.ui-menu-item').each(function (index) {
                if(index === event.target.selectedIndex) {
                    $(this).addClass('activated')
                } else {
                    $(this).removeClass('activated')
                }
            });
        }
    });

    tabSelect.on( "selectmenuchange", function( event, ui ) {
        $('.tabs__content-active').removeClass('tabs__content-active');
        $('.tabs__content[data-content-tab= '+ event.target.value + ']').addClass('tabs__content-active');
    } );

})