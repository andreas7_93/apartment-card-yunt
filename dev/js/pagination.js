$(document).ready(function(){
    let btnNext = $('.pagination__btn-next'),
        btnPrev = $('.pagination__btn-prev'),
        list = $('.pagination__list'),
        listLength = $('.pagination__list__item').length,
        listPosition = 0,
        listItemActive = 0,
        itemMarginRight, widthItem;

    function startPagination (){
        itemMarginRight = $('.pagination__list__item').css('marginRight');
        widthItem = $('.pagination__list__item').width() + parseInt(itemMarginRight, 10);

        for(let i = 0; i < listLength; i++){
            if($('.pagination__list__item')[i].classList.contains('pagination__list__item-active')){
                listItemActive = i + 1;
                if(listItemActive > 0)
                    listPosition = i;
                else
                    listPosition = 0;
            }
        }

        if(listItemActive && listItemActive > 2) {
            listPosition = listItemActive - 3;
            list.css({'transform': 'translateX(' + -widthItem * listPosition + 'px)'});
        }

        if(listItemActive > listLength - 2){
            listPosition = listLength - 5;
            list.css({'transform':'translateX('+ -widthItem * listPosition  +'px)'});
        }

        if(listLength < 5)
            list.css({'transform':'translateX(0px)'})
    }


    startPagination();

    $(window).resize(function(){
        startPagination();
    })

    btnPrev.on('click', function(){
        listPosition--;
        if(listPosition < 0)
            listPosition = 0
        if(listLength < 5)
            return
        list.css({'transform':'translateX('+ -widthItem * listPosition  +'px)'});

    })

    btnNext.on('click', function(){
        listPosition++;
        if(listPosition > listLength - 5)
            listPosition = listLength - 5;
        if(listLength < 5)
            return
        list.css({'transform':'translateX('+ -widthItem * listPosition  +'px)'});
    });

})

